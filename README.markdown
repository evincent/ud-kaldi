This page provides code to perform uncertainty decoding in Kaldi.

# Licence and citation
This software was derived by Yann Salaün and Emmanuel Vincent (Inria) from original Kaldi code and it is distributed under the terms of the [Apache license, version 2.0](http://www.apache.org/licenses/LICENSE-2.0).

If you use this software in a publication, please cite
> Yann Salaün and Emmanuel Vincent, Uncertainty decoding for Kaldi, http://ud-kaldi.gforge.inria.fr/, 2014.

# Download
[kaldi-ud.tar.gz](files/kaldi-ud.tar.gz)

# Install
* Get Kaldi
> svn co -r 1933 https://svn.code.sf.net/p/kaldi/code/trunk kaldi-trunk

Note: This software was developed for version 1933 of Kaldi, that is the version required by the Kaldi baseline for Track 2 of the 2nd CHiME challenge. It may work without change for slightly more recent versions of Kaldi, but it must be adapted in order to work with the latest version.

* Get the archive [kaldi-ud.tar.gz](files/kaldi-ud.tar.gz)
* Untar and run
> ./patch.sh kaldi-trunk
This will add 3 new files and patch the build system.

* Build Kaldi as usual (only the `src/` subdir needs to be rebuilt)

# Use
The result is a new executable: `gmmbin/gmm-latgen-faster-unc` which is identical to `gmmbin/gmm-latgen-faster` except that it takes feature vectors consisting of the concatenation of the mean and the variance of the features as input.
