// decoder/decodable-am-diag-gmm-unc.cc 

// Copyright 2009-2011  Saarland University
//                      Lukas Burget
//                2014  Inria (authors: Yann Salaün and Emmanuel Vincent)

// See kaldi-trunk/COPYING for clarification regarding multiple authors
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//  http://www.apache.org/licenses/LICENSE-2.0
//
// THIS CODE IS PROVIDED *AS IS* BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
// KIND, EITHER EXPRESS OR IMPLIED, INCLUDING WITHOUT LIMITATION ANY IMPLIED
// WARRANTIES OR CONDITIONS OF TITLE, FITNESS FOR A PARTICULAR PURPOSE,
// MERCHANTABLITY OR NON-INFRINGEMENT.
// See the Apache 2 License for the specific language governing permissions and
// limitations under the License.

#include <vector>
using std::vector;

#include "decoder/decodable-am-diag-gmm-unc.h"

namespace kaldi {

BaseFloat DecodableAmDiagGmmUnmappedUnc::LogLikelihoodZeroBased(
    int32 frame, int32 state) {
  KALDI_ASSERT(static_cast<size_t>(frame) < static_cast<size_t>(NumFrames()));
  KALDI_ASSERT(static_cast<size_t>(state) < static_cast<size_t>(NumIndices()));

  if (log_like_cache_[state].hit_time == frame) {
    return log_like_cache_[state].log_like;  // return cached value, if found
  }

  if (frame != previous_frame_) {  // cache the squared stats.
    data_squared_.CopyFromVec(feature_matrix_.Row(frame));
    data_squared_.ApplyPow(2.0);
    previous_frame_ = frame;
  }

  const DiagGmm &pdf = acoustic_model_.GetPdf(state);
  const VectorBase<BaseFloat> &data = feature_matrix_.Row(frame);

  // check if everything is in order
  if (pdf.Dim() != data.Dim()) {
    KALDI_ERR << "Dim mismatch: data dim = "  << data.Dim()
        << " vs. model dim = " << pdf.Dim();
  }
  if (!pdf.valid_gconsts()) {
    KALDI_ERR << "State "  << (state)  << ": Must call ComputeGconsts() "
        "before computing likelihood.";
  }

  int I = pdf.NumGauss();
  int M = data.Dim();

  // sigma_over_sigma is $\frac{\overline{\Sigma}_{n,m}}{\Sigma_{i,m}}$
	// It is needed 3 times in the equation 13 at this URL:
	// https://www.writelatex.com/1221134bggkby#/2943166/
  Matrix<BaseFloat> sigma_over_sigma(pdf.inv_vars());
  Matrix<BaseFloat> tmp(I,M);
  tmp.CopyRowsFromVec(uncertainty_matrix_.Row(frame));
  sigma_over_sigma.MulElements(tmp);
  
  Vector<BaseFloat> loglikes(pdf.gconsts());  // need to recreate for each pdf
  // first member
  Matrix<BaseFloat> first(sigma_over_sigma);
  first.Add(1.0);
  first.ApplyLog();
  Vector<BaseFloat> ones(first.NumCols());
  ones.Set(1.0);
  loglikes.AddMatVec(-0.5, first, kNoTrans, ones, 1.0);

  // second member
  Matrix<BaseFloat> num_first(pdf.means_invvars());
  num_first.MulElements(pdf.means_invvars());
  num_first.DivElements(pdf.inv_vars());
  num_first.MulElements(sigma_over_sigma);

  Matrix<BaseFloat> num_second(pdf.inv_vars());
  tmp.CopyRowsFromVec(data_squared_);
  num_second.MulElements(tmp);
  
  Matrix<BaseFloat> num_third(pdf.means_invvars());
  tmp.CopyRowsFromVec(data);
  num_third.MulElements(tmp);

  Matrix<BaseFloat> num(I,M);
  num.AddMat(0.5, num_first);
  num.AddMat(-0.5, num_second);
  num.AddMat(1.0, num_third);

  Matrix<BaseFloat> denom(sigma_over_sigma);
  denom.Add(1.0);

  Matrix<BaseFloat> second(num);
  second.DivElements(denom);
  loglikes.AddMatVec(1.0, second, kNoTrans, ones, 1.0);

  BaseFloat log_sum = loglikes.LogSumExp();
  if (KALDI_ISNAN(log_sum) || KALDI_ISINF(log_sum))
    KALDI_ERR << "Invalid answer (overflow or invalid variances/features?)";

  log_like_cache_[state].log_like = log_sum;
  log_like_cache_[state].hit_time = frame;

  return log_sum;
}

void DecodableAmDiagGmmUnmappedUnc::ResetLogLikeCache() {
  if (static_cast<int32>(log_like_cache_.size()) != acoustic_model_.NumPdfs()) {
    log_like_cache_.resize(acoustic_model_.NumPdfs());
  }
  vector<LikelihoodCacheRecord>::iterator it = log_like_cache_.begin(),
      end = log_like_cache_.end();
  for (; it != end; ++it) { it->hit_time = -1; }
}

}  // namespace kaldi
