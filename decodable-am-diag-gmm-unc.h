// decoder/decodable-am-diag-gmm-unc.h 

// Copyright 2009-2011  Microsoft Corporation
//                      Saarland University
//                      Lukas Burget
//                2014  Inria (authors: Yann Salaün and Emmanuel Vincent)

// See kaldi-trunk/COPYING for clarification regarding multiple authors
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//  http://www.apache.org/licenses/LICENSE-2.0
//
// THIS CODE IS PROVIDED *AS IS* BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
// KIND, EITHER EXPRESS OR IMPLIED, INCLUDING WITHOUT LIMITATION ANY IMPLIED
// WARRANTIES OR CONDITIONS OF TITLE, FITNESS FOR A PARTICULAR PURPOSE,
// MERCHANTABLITY OR NON-INFRINGEMENT.
// See the Apache 2 License for the specific language governing permissions and
// limitations under the License.

ifndef KALDI_DECODER_DECODABLE_AM_DIAG_GMM_UNC_H_
#define KALDI_DECODER_DECODABLE_AM_DIAG_GMM_UNC_H_

#include <vector>

#include "base/kaldi-common.h"
#include "gmm/am-diag-gmm.h"
#include "hmm/transition-model.h"
#include "itf/decodable-itf.h"
#include "transform/regression-tree.h"
#include "transform/regtree-fmllr-diag-gmm.h"
#include "transform/regtree-mllr-diag-gmm.h"

namespace kaldi {

class DecodableAmDiagGmmUnmappedUnc : public DecodableInterface {
 public:
  DecodableAmDiagGmmUnmappedUnc(const AmDiagGmm &am,
                             const MatrixBase<BaseFloat> &feats,
                             const MatrixBase<BaseFloat> &uncertainty)
      : acoustic_model_(am), feature_matrix_(feats), uncertainty_matrix_(uncertainty),
        previous_frame_(-1), data_squared_(feats.NumCols()) {
    ResetLogLikeCache();
  }

  // Note, frames are numbered from zero.  But state_index is numbered
  // from one (this routine is called by FSTs).
  virtual BaseFloat LogLikelihood(int32 frame, int32 state_index) {
    return LogLikelihoodZeroBased(frame, state_index - 1);
  }
  int32 NumFrames() { return feature_matrix_.NumRows(); }
  
  // Indices are one-based!  This is for compatibility with OpenFst.
  virtual int32 NumIndices() { return acoustic_model_.NumPdfs(); }

  virtual bool IsLastFrame(int32 frame) {
    KALDI_ASSERT(frame < NumFrames());
    return (frame == NumFrames() - 1);
  }

 protected:
  void ResetLogLikeCache();
  virtual BaseFloat LogLikelihoodZeroBased(int32 frame, int32 state_index);

  const AmDiagGmm &acoustic_model_;
  const MatrixBase<BaseFloat> &feature_matrix_;
  const MatrixBase<BaseFloat> &uncertainty_matrix_;
  int32 previous_frame_;

  /// Defines a cache record for a state
  struct LikelihoodCacheRecord {
    BaseFloat log_like;  ///< Cache value
    int32 hit_time;     ///< Frame for which this value is relevant
  };
  std::vector<LikelihoodCacheRecord> log_like_cache_;

 private:
  Vector<BaseFloat> data_squared_;  ///< Cache for fast likelihood calculation

  KALDI_DISALLOW_COPY_AND_ASSIGN(DecodableAmDiagGmmUnmappedUnc);
};

class DecodableAmDiagGmmScaledUnc: public DecodableAmDiagGmmUnmappedUnc {
 public:
  DecodableAmDiagGmmScaledUnc(const AmDiagGmm &am,
                           const TransitionModel &tm,
                           const MatrixBase<BaseFloat> &feats,
                           const MatrixBase<BaseFloat> &uncertainty,
                           BaseFloat scale)
      : DecodableAmDiagGmmUnmappedUnc(am, feats, uncertainty), trans_model_(tm),
        scale_(scale) {}

  // Note, frames are numbered from zero but transition-ids from one.
  virtual BaseFloat LogLikelihood(int32 frame, int32 tid) {
    return scale_*LogLikelihoodZeroBased(frame,
                                         trans_model_.TransitionIdToPdf(tid));
  }
  // Indices are one-based!  This is for compatibility with OpenFst.
  virtual int32 NumIndices() { return trans_model_.NumTransitionIds(); }

  const TransitionModel *TransModel() { return &trans_model_; }
 private: // want to access it public to have pdf id information
  const TransitionModel &trans_model_;  // for transition-id to pdf mapping
  BaseFloat scale_;
  KALDI_DISALLOW_COPY_AND_ASSIGN(DecodableAmDiagGmmScaledUnc);
};
}  // namespace kaldi

#endif  // KALDI_DECODER_DECODABLE_AM_DIAG_GMM_UNC_H_
