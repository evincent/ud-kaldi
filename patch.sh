#!/bin/bash

if [ "$#" -ne "1" ]
then
	echo "Usage: $0 kaldi-trunk-dir"
	exit 1
fi

KALDI_TRUNK=$1

cp decodable-am-diag-gmm-unc.cc $KALDI_TRUNK/src/decoder/
cp decodable-am-diag-gmm-unc.h $KALDI_TRUNK/src/decoder/
cp gmm-latgen-faster-unc.cc $KALDI_TRUNK/src/gmmbin/
patch -p0 -d $KALDI_TRUNK/src < makefiles.patch

echo "Done! You can now build kaldi"
